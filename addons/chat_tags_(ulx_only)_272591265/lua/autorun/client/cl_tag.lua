
-----------------------------------------------------
// Client Side
// Originally Made By: TyGuy
// Edited By: CaptainDeagle62

local Tags = 
{
--Group    --Tag     --Color
{"superadmin",      "[Начальник метрополитена]", Color(255,   0,   0, 255) },
{"chiefinstructor", "[Главный инструктор]",      Color(255, 100,   0, 255) },
{"auditor",         "[Ревизор]",                 Color(255, 100,   0, 255) },
{"instructor",      "[Машинист инструктор]",     Color(255, 150,   0, 255) },
{"admin",           "[Поездной диспетчер]",      Color(255, 150,   0, 255) },
{"driver1class",    "[Машинист 1го класса]",     Color(  0, 120, 255, 255) },
{"driver2class",    "[Машинист 2го класса]",     Color(  0, 220,  50, 255) },
{"driver3class",    "[Машинист 3го класса]",     Color(100, 200,   0, 255) },
{"user",            "[Помощник машиниста]",      Color(240, 240, 100, 255) },
}

hook.Add("OnPlayerChat", "Tags", function(ply, Text, Team, PlayerIsDead)
	if ply:IsValid() then
		for k,v in pairs(Tags) do
			if ply:IsUserGroup(v[1]) then
				if Team then
					if ply:Alive() then
						chat.AddText(v[3], v[2], " ", Color(240, 240, 100, 255), ply:Nick(), color_white, ": ", Color(255, 255, 255, 255), Text)
					else
						chat.AddText(Color(255, 0, 0, 255), "*DEAD*", v[3], v[2], " ", Color(240, 240, 100, 255), ply:Nick(), color_white, ": ", Color(255, 255, 255, 255), Text)
					end
					return true
				end
				if ply:IsPlayer() then
					if ply:Alive() then
						chat.AddText(v[3], v[2], " ", Color(240, 240, 100, 255), ply:Nick(), color_white, ": ", Color(255, 255, 255, 255), Text)
						return true
					elseif !ply:Alive() then
						chat.AddText(Color(255, 0, 0, 255), "*DEAD* ", v[3], v[2], " ", Color(240, 240, 100, 255), ply:Nick(), color_white, ": ", Color(255, 255, 255, 255), Text)
						return true
					end
				end
			end
		end
	end
end)