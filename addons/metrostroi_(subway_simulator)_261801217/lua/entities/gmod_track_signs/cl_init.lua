
-----------------------------------------------------
include("shared.lua")

function ENT:Initialize()
	--self.ModelProp = self:GetNWInt("Model")
	hook.Add("PostDrawOpaqueRenderables", "metrostroi_sign_debug_draw_"..self:EntIndex(), function()
			if GetConVarNumber("metrostroi_drawdebug") == 0 then return end
			--print(2)
			local pos = self:LocalToWorld(Vector(0,0,0))
			local ang = self:LocalToWorldAngles(Angle(0,90,90))
			cam.Start3D2D(pos , ang, 0.25)
				surface.SetDrawColor(125, 125, 0, 255)
				surface.DrawRect(-40, -20, 80, 20)
			cam.End3D2D()
	end )
end

function ENT:OnRemove()
	hook.Remove("PostDrawOpaqueRenderables", "metrostroi_sign_debug_draw_"..self:EntIndex())	
	if IsValid(self.Model) then self.Model:Remove() end
	self.Model = nil
end

function ENT:Think()
	if LocalPlayer():GetPos():Distance(self:GetPos()) > 10000 or LocalPlayer():GetPos().z - self:GetPos().z > 500 then
		if IsValid(self.Model) then self.Model:Remove() end
		self.Model= nil
		return
	end
	if self:GetNWInt("Type") - 2 < 0 then return end
	self.ModelProp = self.SignModels[self:GetNWInt("Type") - 2]
	self.Offset = self:GetNWVector("Offset")
	--[[
	if self.NotInitalised then return end
	if ((self.LightType > 1 and self.Lenses == "") or (self.LightType == 1 and self.Lenses ~= "")
	or (self.LightType > 1 and self.Name == "") or (self.LightType == 1 and self.Name ~= "")
	or (self.Name ~= "" and self.Lenses == "") or (self.Name == "" and self.Lenses ~= "") or self.LightType < 1)
	and not self.sended then
		--print(self.Name..", i lose all fucking NWVars!")
		net.Start("metrostroi-signal-need-update")
			net.WriteEntity(self)
		net.SendToServer()
		self.sended = true
		timer.Simple(1.5,function() self.sended = false end)
	end
	]]
	if not IsValid(self.Model) then
		self.Model = ClientsideModel(self.ModelProp.model,RENDERGROUP_OPAQUE)
		self.Model:SetParent(self)
		self.Model:SetPos(self:LocalToWorld(self.ModelProp.pos) + self.Offset)
		self.Model:SetAngles(self:LocalToWorldAngles(self.ModelProp.angles))
	end
end

function ENT:Draw()
	--self:DrawModel()
end