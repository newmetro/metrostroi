
-----------------------------------------------------
include("shared.lua")


--------------------------------------------------------------------------------
function ENT:Initialize()
	self.OldName = ""
	self.Models = {}
	self.NotInitalised = true
	timer.Simple(0.55, function()
		self.NotInitalised = nil
	end)
end
function ENT:OnRemove()
	hook.Remove("PostDrawOpaqueRenderables", "metrostroi_signal_draw_"..self:EntIndex())	
	self.CLDraw = false
	self.OldName = ""
	self:RemoveModels()
	--hook.Remove("PostDrawOpaqueRenderables")
end
function ENT:RemoveModels()
	for k,v in pairs(self.Models) do v:Remove() end
	self.Models = {}
end

function ENT:Think()
	if LocalPlayer():GetPos():Distance(self:GetPos()) > 10000 or LocalPlayer():GetPos().z - self:GetPos().z > 500 then
		for k,v in pairs(self.Models) do
			if IsValid(v) then
				v:Remove()
			end
		end
		if #self.Models > 0 then
			self.Models = {}
		end
		return
	end
	self.LightType = self:GetNWInt("LightType")
	self.Name = self:GetNWString("Name")
	self.Lenses = self:GetNWString("Lenses")
	if self.NotInitalised then return end
	if ((self.LightType > 1 and self.Lenses == "") or (self.LightType == 1 and self.Lenses ~= "")
	or (self.LightType > 1 and self.Name == "") or (self.LightType == 1 and self.Name ~= "")
	or (self.Name ~= "" and self.Lenses == "") or (self.Name == "" and self.Lenses ~= "") or self.LightType < 1)
	and not self.sended then
		--print(self.Name..", i lose all fucking NWVars!")
		net.Start("metrostroi-signal-need-update")
			net.WriteEntity(self)
		net.SendToServer()
		self.sended = true
		timer.Simple(1.5,function() self.sended = false end)
	end
	self.LightType = self.LightType - 2
	--self.RouteNumber = self:GetNWString("RouteNumber")
	local models = self.TrafficLightModels[self.LightType] or {}
	local ID = 0
	-- Create new clientside models
	if not self.OnlyARS then
		if self.Lenses ~= self.OldLenses then
			for k,v in pairs(self.Models) do
				if IsValid(v) then
					v:Remove()
				end
				self.Models[k] = nil
			end
		end
		if not self.LensesTBL or self.Lenses ~= self.OldLenses then
			self.LensesTBL = string.Explode("-",self.Lenses)
		end
		for k,v in pairs(models) do
			if type(v) == "string" then
				if not IsValid(self.Models[v]) then
					self.Models[v] = ClientsideModel(v,RENDERGROUP_OPAQUE)
					self.Models[v]:SetPos(self:LocalToWorld(self.BasePosition))
					self.Models[v]:SetAngles(self:GetAngles())
					self.Models[v]:SetParent(self)
				end
			end
		end
		self.NamesOffset = Vector(0,0,0)
		-- Create traffic light models
		if self.LightType > 2 then self.LightType = 2 end
		if self.LightType < 0 then self.LightType = 0 end

		local offset = self.RenderOffset[self.LightType] or Vector(0,0,0)
		for k,v in ipairs(self.LensesTBL) do
			if not IsValid(self.Models[ID]) then
				local data	
				if v ~= "M" then
					data = #v ~= 1 and self.TrafficLightModels[self.LightType][#v-1] or self.TrafficLightModels[self.LightType][Metrostroi.Signal_IS]
				else
					data = self.TrafficLightModels[self.LightType][Metrostroi.Signal_RP]
				end
				if not data then continue end
				offset = offset - Vector(0,0,data[1])
				self.NamesOffset = self.NamesOffset + Vector(0,0,data[1])

				self.Models[ID] = ClientsideModel(data[2],RENDERGROUP_OPAQUE)
				self.Models[ID]:SetPos(self:LocalToWorld(self.BasePosition + offset))
				self.Models[ID]:SetAngles(self:GetAngles())
				self.Models[ID]:SetParent(self)
			end
			ID = ID + 1
		end
	else
		local k = "m1"
		local v = self.TrafficLightModels[self.LightType]["m1"]

		if not IsValid(self.Models[k]) then
			self.Models[k] = ClientsideModel(v,RENDERGROUP_OPAQUE)
			self.Models[k]:SetPos(self:LocalToWorld(self.BasePosition))
			self.Models[k]:SetAngles(self:GetAngles())
			self.Models[k]:SetParent(self)
		end
	end

	for i = 0,#self.Name-1 do
		if self.Name[i+1] == " " then continue end
		if not IsValid(self.Models["names_"..i]) then
			self.OldName = ""
			break
		end
	end

	if self.OldName ~= self.Name then
		for i = 0,#self.Name-1 do
			if self.Name[i+1] == " " then continue end
			if self.Models["names_"..i] then
				self.Models["names_"..i]:Remove()
				self.Models["names_"..i] = nil
			end
		end

		local offset = (self.RenderOffset[self.LightType] or Vector(0,0,0)) + (self.TrafficLightModels[self.LightType]["name"] or Vector(0,0,0))
		if self.LightType == 1 then
			offset = offset - self.NamesOffset
		end
		for i = 0,#self.Name-1 do
			if self.Name[i+1] == " " then continue end
			if not IsValid(self.Models["names_"..i]) then
				self.Models["names_"..i] = ClientsideModel("models/metrostroi/signals/letter.mdl",RENDERGROUP_OPAQUE)
				self.Models["names_"..i]:SetAngles(self:GetAngles())
				self.Models["names_"..i]:SetPos(self:LocalToWorld(self.BasePosition + offset - Vector(0,0,i*5.5)))
				self.Models["names_"..i]:SetParent(self)
			end
		end
	end

	self.OldLenses = self.Lenses
	self.OldName = self.Name
end

function ENT:Draw()
	-- Draw model
	self:DrawModel()
	if not self.CLDraw then
		hook.Add("PostDrawOpaqueRenderables", "metrostroi_signal_draw_"..self:EntIndex(), function()
			if not IsValid(self) or not self.Name then return end
			self.CLDraw = true

			if LocalPlayer():GetPos():Distance(self:GetPos()) > 1000 then return end

			for i = 0,#self.Name-1 do
				if self.Name[i+1] == " " or not IsValid(self.Models["names_"..i]) then continue end

				local pos = self.Models["names_"..i]:LocalToWorld(Vector(0,0.3,0))
				local angle = self.Models["names_"..i]:LocalToWorldAngles(Angle(0,180,90))
				local offset = (self.RenderOffset[self.LightType] or Vector(0,0,0)) + (self.TrafficLightModels[self.LightType]["name"] or Vector(0,0,0))
				if self.LightType == 1 then
					for i = 1,#self.TrafficLightModels[self.LightType] do
						offset = offset - Vector(0,0,self.TrafficLightModels[self.LightType][i][1])
					end
				end

				cam.Start3D2D( pos, angle, 0.25 )
					surface.SetTextColor( Color(0,0,0) )
					surface.SetFont("CloseCaption_Bold")
					local x,y = surface.GetTextSize(self.Name[i+1])
					surface.SetTextPos(-x/2, -y/2)
					surface.DrawText(self.Name[i+1])
				cam.End3D2D()
			end
		end)
	end
end