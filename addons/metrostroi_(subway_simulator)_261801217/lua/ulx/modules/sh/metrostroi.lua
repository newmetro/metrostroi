
-----------------------------------------------------
local CATEGORY_NAME = "Metrostroi"

------------------------------ Wagons ------------------------------
local waittime = 10
local lasttimeusage = -waittime
function ulx.wagons( calling_ply )
	if lasttimeusage + waittime > CurTime() then
		ULib.tsayError( calling_ply, "Please wait " .. math.Round(lasttimeusage + waittime - CurTime()) .. " seconds before using this command again", true )
		return
	end

	lasttimeusage = CurTime()

	ulx.fancyLog("Wagons on server: #s", Metrostroi.TrainCount())
	if CPPI then
		local N = {}
		for k,v in pairs(Metrostroi.TrainClasses) do
			if  v == "gmod_subway_base" then continue end
			local ents = ents.FindByClass(v)
			for k2,v2 in pairs(ents) do
				N[v2:CPPIGetOwner() or v2:GetNetworkedEntity("Owner", "N/A") or "(disconnected)"] = (N[v2:CPPIGetOwner() or v2:GetNetworkedEntity("Owner", "N/A") or "(disconnected)"] or 0) + 1
			end
		end
		for k,v in pairs(N) do
			ulx.fancyLog("#s wagons have #s",v,(type(k) == "Player" and IsValid(k)) and k:GetName() or k)
		end
	end
	ulx.fancyLog("Max trains: #s.\nMax wagons: #s.\nMax trains per player: #s", GetConVarNumber("metrostroi_maxtrains"), GetConVarNumber("metrostroi_maxwagons"), GetConVarNumber("metrostroi_maxtrains_onplayer"))
end
local wagons = ulx.command( CATEGORY_NAME, "ulx trains", ulx.wagons, "!trains" )
wagons:defaultAccess( ULib.ACCESS_ALL )
wagons:help( "Shows you the current wagons." )

------------------------------ Trainfuck ------------------------------
local Models = {
	"models/metrostroi/81/81-7036.mdl",
	"models/metrostroi/81/81-7037.mdl",
	"models/metrostroi/81/81-714.mdl",
	"models/metrostroi/81/81-717a.mdl",
	"models/metrostroi/81/81-717b.mdl",
	"models/metrostroi/81/ema508t.mdl",
	"models/metrostroi/e/em508.mdl",
	"models/metrostroi/tatra_t3/tatra_t3.mdl",
	"models/props_trainstation/train001.mdl",
	"models/props_combine/CombineTrain01a.mdl",
	"models/props_combine/combine_train02a.mdl",
}
local function SpawnTrain( Pos, Direction )
        local train = ents.Create( "prop_physics" )
        local random = math.random(1,#Models)
        train:SetModel(Models[random])
        train:SetAngles( Direction:Angle() + Angle(0,string.find(Models[random],"metrostroi") and 0 or 270,0) )
        train:SetPos( Pos )
        if math.random() > 0.6 then train:SetColor( Color(math.random(0,255),math.random(0,255),math.random(0,255)) ) end
        train:SetSkin(math.random(0,2))
        train:Spawn()
        train:Activate()
        train:EmitSound( "ambient/alarms/train_horn2.wav", 100, 100 )
        train:GetPhysicsObject():SetVelocity( Direction * math.random(100000,500000) )
       
        --timer.Create( "TrainRemove_"..CurTime(), 5, 1, function( train ) train:Remove() end, train )
        timer.Simple( 5, function() train:Remove() end )
end

function ulx.trainfuck(calling_ply, target_plys)
	local affected_plys = {}

	for i=1, #target_plys do
		local v = target_plys[ i ]

		if ulx.getExclusive( v, calling_ply ) then
			ULib.tsayError( calling_ply, ulx.getExclusive( v, calling_ply ), true )
		elseif not v:Alive() then
			ULib.tsayError( calling_ply, v:Nick() .. " is already dead!", true )
		elseif v:IsFrozen() then
			ULib.tsayError( calling_ply, v:Nick() .. " is frozen!", true )
		else
			v:SetMoveType( MOVETYPE_WALK )
			SpawnTrain( v:GetPos() + v:GetForward() * 1000 + Vector(0,0,120), v:GetForward() * -1 )
			table.insert( affected_plys, v )
		end
	end

	ulx.fancyLogAdmin( calling_ply, "#A trainfucked #T", affected_plys )
end
local traunfuck = ulx.command( "Fun", "ulx trainfuck", ulx.trainfuck, "!trainfuck", true )
traunfuck:addParam{ type=ULib.cmds.PlayersArg }
traunfuck:defaultAccess( ULib.ACCESS_ADMIN )
traunfuck:help( "Trainfucks a player." )