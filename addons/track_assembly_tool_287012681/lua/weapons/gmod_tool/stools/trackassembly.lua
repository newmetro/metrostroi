
-----------------------------------------------------
---------------- Localizing Libraries ----------------

local type                  = type
local Color                 = Color
local pairs                 = pairs
local print                 = print
local Angle                 = Angle
local Vector                = Vector
local ipairs                = ipairs
local IsValid               = IsValid
local tostring              = tostring
local tonumber              = tonumber
local LocalPlayer           = LocalPlayer
local GetConVarString       = GetConVarString
local GetConVarNumber       = GetConVarNumber
local RunConsoleCommand     = RunConsoleCommand
local RestoreCursorPosition = RestoreCursorPosition
local os                    = os
local sql                   = sql
local undo                  = undo
local util                  = util
local math                  = math
local ents                  = ents
local file                  = file
local string                = string
local cleanup               = cleanup
local duplicator            = duplicator
local constraint            = constraint
local concommand            = concommand

----------------- TOOL Global Parameters ----------------

--- ZERO Objects
local VEC_ZERO = Vector(0,0,0)
local ANG_ZERO = Angle (0,0,0)

--- Toolgun Background texture ID reference
local txToolgunBackground

--- Panels and stuff

local pnTrackAssemblyFrame
local pnTrackAssemblyButtonL
local pnTrackAssemblyButtonR
local pnTrackAssemblyListView
local pnTrackAssemblyModelPanel
local pnTrackAssemblyProgressBar

--- Render Base Colors
local stDrawDyes = {
  Red   = Color(255, 0 , 0 ,255),
  Green = Color( 0 ,255, 0 ,255),
  Blue  = Color( 0 , 0 ,255,255),
  Cyan  = Color( 0 ,255,255,255),
  Magen = Color(255, 0 ,255,255),
  Yello = Color(255,255, 0 ,255),
  White = Color(255,255,255,255),
  Black = Color( 0 , 0 , 0 ,255),
  Ghost = Color(255,255,255,150),
  Txtsk = Color(161,161,161,255)
}

--- Because Vec[1] is actually faster than Vec.X
--- Vector Component indexes ---
local cvX = 1
local cvY = 2
local cvZ = 3

--- Angle Component indexes ---
local caP = 1
local caY = 2
local caR = 3

--- Component Status indexes ---
-- Sign of the first component
local csX = 1
-- Sign of the second component
local csY = 2
-- Sign of the third component
local csZ = 3
-- Flag for disabling the point
local csD = 4

TOOL.Category   = "Construction"    -- Name of the category
TOOL.Name       = "#Track Assembly" -- Name to display
TOOL.Command    = nil               -- Command on click (nil for default)
TOOL.ConfigName = ""                -- Config file name (nil for default)

TOOL.ClientConVar = {
  [ "weld"      ] = "1",
  [ "mass"      ] = "25000",
  [ "model"     ] = "models/props_phx/trains/tracks/track_1x.mdl",
  [ "nextx"     ] = "0",
  [ "nexty"     ] = "0",
  [ "nextz"     ] = "0",
  [ "count"     ] = "1",
  [ "freeze"    ] = "0",
  [ "advise"    ] = "1",
  [ "igntyp"    ] = "0",
  [ "weldgnd"   ] = "0",
  [ "flatten"   ] = "0",
  [ "ydegsnp"   ] = "0",
  [ "pointid"   ] = "1",
  [ "pnextid"   ] = "1",
  [ "nextpic"   ] = "0",
  [ "nextyaw"   ] = "0",
  [ "nextrol"   ] = "0",
  [ "enghost"   ] = "0",
  [ "addinfo"   ] = "0",
  [ "logsenb"   ] = "0",
  [ "logsmax"   ] = "10000",
  [ "logfile"   ] = "trackasmlib_log",
  [ "mcspawn"   ] = "1",
  [ "bgskids"   ] = "",
  [ "activrad"  ] = "30",
  [ "autoffsz"  ] = "1",
  [ "exportdb"  ] = "0",
  [ "maxstatts" ] = "3",
  [ "nocollide" ] = "0",
  [ "engravity" ] = "1",
  [ "physmater" ] = "metal",
}

------------- LOCAL FUNCTIONS AND STUFF ----------------

if(CLIENT) then
  language.Add("Tool."   ..trackasmlib.GetToolNameL()..".name", "Track Assembly")
  language.Add("Tool."   ..trackasmlib.GetToolNameL()..".desc", "Assembles a track for vehicles to run on")
  language.Add("Tool."   ..trackasmlib.GetToolNameL()..".0"   , "Left Click to continue the track, Right to change active position, Reload to remove a piece")
  language.Add("Cleanup."..trackasmlib.GetToolNameL()         , "Track Assembly")
  language.Add("Cleaned."..trackasmlib.GetToolNameL().."s"    , "Cleaned up all Pieces")

  local function ResetTrackAssemblyOffsets(oPly,oCom,oArgs)
    -- Reset all of the offset options to zero
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nextpic 0\n")
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nextyaw 0\n")
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nextrol 0\n")
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nextx 0\n")
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nexty 0\n")
    oPly:ConCommand(trackasmlib.GetToolPrefixL().."nextz 0\n")
  end
  concommand.Add(trackasmlib.GetToolPrefixL().."resetoffs", ResetTrackAssemblyOffsets)
  
  local function OpenTrackAssemblyFrame(oPly,oCom,oArgs)
    local Ind = 1
    local CntN = tonumber(oArgs[1]) or 0
    local scrW = surface.ScreenWidth()
    local scrH = surface.ScreenHeight()
    local FrequentlyUsed = trackasmlib.GetFrequentlyUsed(CntN)
    if(not FrequentlyUsed) then
      oPly:ChatPrint(trackasmlib.GetToolNameU()..": Failed to retrieve most frequent models")
      return
    end
    if(not IsValid(pnTrackAssemblyFrame)) then
      pnTrackAssemblyFrame = vgui.Create("DFrame")
      pnTrackAssemblyFrame:SetTitle("Frequently used pieces by "..oPly:Nick())
      pnTrackAssemblyFrame:SetVisible(false)
      pnTrackAssemblyFrame:SetDraggable(true)
      pnTrackAssemblyFrame:SetDeleteOnClose(true)
      pnTrackAssemblyFrame:SetPos(scrW/4, scrH/4)
      pnTrackAssemblyFrame:SetSize(750, 280)
      pnTrackAssemblyFrame.OnClose = function()
        pnTrackAssemblyFrame:SetVisible(false)
        pnTrackAssemblyButtonL:Remove()
        pnTrackAssemblyButtonR:Remove()
        pnTrackAssemblyListView:Remove()
        pnTrackAssemblyModelPanel:Remove()
        pnTrackAssemblyProgressBar:Remove()
      end
    end
    if(not IsValid(pnTrackAssemblyModelPanel)) then
      pnTrackAssemblyModelPanel = vgui.Create("DModelPanel",pnTrackAssemblyFrame)
      pnTrackAssemblyModelPanel:SetParent(pnTrackAssemblyFrame)
      pnTrackAssemblyModelPanel:SetPos(500,25)
      pnTrackAssemblyModelPanel:SetSize(250, 255)
      pnTrackAssemblyModelPanel:SetVisible(false)
      pnTrackAssemblyModelPanel.LayoutEntity = function(pnSelf, oEnt)
        if(pnSelf.bAnimated) then
          pnSelf:RunAnimation()
        end
        local uiRec = trackasmlib.CacheQueryPiece(oEnt:GetModel())
        if(not uiRec) then return end
        local Ang = Angle(0, RealTime() * 10, 0)
        local Pos = trackasmlib.GetCenterPoint(uiRec)
        local Rot = Vector()
              Rot:Set(Pos)
              Rot:Rotate(Ang)
              Rot:Mul(-1)
              Rot:Add(Pos)
        oEnt:SetAngles(Ang) 
        oEnt:SetPos(Rot)
      end
    end
    if(not IsValid(pnTrackAssemblyProgressBar)) then
      pnTrackAssemblyProgressBar = vgui.Create("DProgress", pnTrackAssemblyFrame)
      pnTrackAssemblyProgressBar:SetParent(pnTrackAssemblyFrame)
      pnTrackAssemblyProgressBar:SetVisible(false)
      pnTrackAssemblyProgressBar:SetPos(10, 30)
      pnTrackAssemblyProgressBar:SetSize(490, 30)
      pnTrackAssemblyProgressBar:SetFraction(0)
    end
    if(not IsValid(pnTrackAssemblyButtonL)) then
      pnTrackAssemblyButtonL = vgui.Create("DButton", pnTrackAssemblyFrame)
      pnTrackAssemblyButtonL:SetParent(pnTrackAssemblyFrame)
      pnTrackAssemblyButtonL:SetText("Export client's DB")
      pnTrackAssemblyButtonL:SetPos(15,30)
      pnTrackAssemblyButtonL:SetSize(230,30)
      pnTrackAssemblyButtonL.DoClick = function()
        local exportdb = GetConVarNumber(trackasmlib.GetToolPrefixL().."exportdb") or 0
        if(exportdb ~= 0) then
          trackasmlib.Log("Button: "..pnTrackAssemblyButtonL:GetText())
          trackasmlib.ExportSQL2Lua    ("PIECES")
          trackasmlib.ExportSQL2Inserts("PIECES")
          trackasmlib.ExportSQL2Lua    ("ADDITIONS")
          trackasmlib.ExportSQL2Inserts("ADDITIONS")
          trackasmlib.ExportSQL2Lua    ("PHYSPROPERTIES")
          trackasmlib.ExportSQL2Inserts("PHYSPROPERTIES")
          trackasmlib.SQLExportIntoDSV ("PIECES","\t")
          trackasmlib.SQLExportIntoDSV ("ADDITIONS","\t")
          trackasmlib.SQLExportIntoDSV ("PHYSPROPERTIES","\t")
        end
      end
    end
    if(not IsValid(pnTrackAssemblyButtonR)) then
      pnTrackAssemblyButtonR = vgui.Create("DButton", pnTrackAssemblyFrame)
      pnTrackAssemblyButtonR:SetParent(pnTrackAssemblyFrame)
      pnTrackAssemblyButtonR:SetText("Set client's LOG control")
      pnTrackAssemblyButtonR:SetPos(255,30)
      pnTrackAssemblyButtonR:SetSize(230,30)
      pnTrackAssemblyButtonR.DoClick = function()
        trackasmlib.Log("Button: "..pnTrackAssemblyButtonR:GetText())
        local logsmax  = GetConVarNumber(trackasmlib.GetToolPrefixL().."logsmax") or 0
        if(logsmax > 0) then
          local logsenb = GetConVarNumber(trackasmlib.GetToolPrefixL().."logsenb") or 0
          local logfile = GetConVarString(trackasmlib.GetToolPrefixL().."logfile") or ""
          trackasmlib.SetLogControl(logsenb,logsmax,logfile)
        end
      end
    end
    if(not IsValid(pnTrackAssemblyListView)) then
      pnTrackAssemblyListView = vgui.Create("DListView", pnTrackAssemblyFrame)
      pnTrackAssemblyListView:SetParent(pnTrackAssemblyFrame)
      pnTrackAssemblyListView:SetVisible(false)
      pnTrackAssemblyListView:SetMultiSelect(false)
      pnTrackAssemblyListView:SetPos(10,65)
      pnTrackAssemblyListView:SetSize(480,205)
      pnTrackAssemblyListView:AddColumn("Life"):SetFixedWidth(55)
      pnTrackAssemblyListView:AddColumn("Act"):SetFixedWidth(20)
      pnTrackAssemblyListView:AddColumn("Type"):SetFixedWidth(100)
      pnTrackAssemblyListView:AddColumn("Model"):SetFixedWidth(305)
      pnTrackAssemblyListView.OnRowSelected = function(pnSelf, nRow, pnVal)
        local uiMod = FrequentlyUsed[nRow].Table[3]
        pnTrackAssemblyModelPanel:SetModel(uiMod)
        local uiRec = trackasmlib.CacheQueryPiece(uiMod)
        if(not uiRec) then return end
        -- OBBCenter ModelPanel Configuration --
        local uiEnt = pnTrackAssemblyModelPanel.Entity
        local uiCen = trackasmlib.GetCenterPoint(uiRec)
        local uiEye = uiEnt:LocalToWorld(uiCen)
        trackasmlib.SubVector(uiCen,uiRec.Offs[1].P)
        local uiLen = uiCen:Length()
        local uiCam = Vector(0.70 * uiLen, 0, 0.30 * uiLen)
        pnTrackAssemblyModelPanel:SetLookAt(uiEye)
        pnTrackAssemblyModelPanel:SetCamPos(2 * uiCam + uiEye)
        oPly:ConCommand(trackasmlib.GetToolPrefixL().."model "..uiMod.."\n")
        oPly:ConCommand(trackasmlib.GetToolPrefixL().."pointid 1\n")
        oPly:ConCommand(trackasmlib.GetToolPrefixL().."pnextid 2\n")
      end
    end
    pnTrackAssemblyFrame:SetVisible(true)
    pnTrackAssemblyFrame:Center()
    pnTrackAssemblyFrame:MakePopup()
    pnTrackAssemblyListView:Clear()
    pnTrackAssemblyListView:SetVisible(false)
    pnTrackAssemblyProgressBar:SetVisible(true)
    pnTrackAssemblyProgressBar:SetFraction(0)
    RestoreCursorPosition()
    while(FrequentlyUsed[Ind]) do
      local Val = FrequentlyUsed[Ind]
      local Rec = trackasmlib.CacheQueryPiece(Val.Table[3])
      if(Rec) then
        pnTrackAssemblyListView:AddLine(
          trackasmlib.RoundValue(Val.Value,0.001),
            Val.Table[1],Val.Table[2],Val.Table[3])
      end
      pnTrackAssemblyProgressBar:SetFraction(Ind/CntN)
      Ind = Ind + 1
    end
    pnTrackAssemblyListView:SetVisible(true)
    pnTrackAssemblyProgressBar:SetVisible(false)
    pnTrackAssemblyButtonL:SetVisible(true)
    pnTrackAssemblyButtonL:SetVisible(true)
    pnTrackAssemblyModelPanel:SetVisible(true)
    RememberCursorPosition()
  end
  concommand.Add(trackasmlib.GetToolPrefixL().."openframe", OpenTrackAssemblyFrame)
  
  txToolgunBackground = surface.GetTextureID("vgui/white")
end

if(SERVER) then

  cleanup.Register(trackasmlib.GetToolNameU().."s")

  function LoadDupeTrackAssemblyWeldGround(Ply,oEnt,tData)
    if(tData[1]) then
      oEnt:SetMoveType(MOVETYPE_NONE)
      oEnt:SetUnFreezable(true)
      oEnt.PhysgunDisabled = true
      oEnt:GetPhysicsObject():EnableMotion(false)
      duplicator.StoreEntityModifier(oEnt,trackasmlib.GetToolPrefixL().."weldgnd", {[1] = true})
    end
  end
  duplicator.RegisterEntityModifier(trackasmlib.GetToolPrefixL().."weldgnd",
                                    LoadDupeTrackAssemblyWeldGround)

  function eMakeTrackAssemblyPiece(sModel,vPos,aAng,nMass,sBgSkIDs)
    -- You never know .. ^_^
    if(not util.IsValidModel(sModel)) then return nil end
    local stPiece = trackasmlib.CacheQueryPiece(sModel)
    if(not stPiece) then return nil end -- Not present in the DB
    local ePiece = ents.Create("prop_physics")
    if(ePiece and ePiece:IsValid()) then
      ePiece:SetCollisionGroup(COLLISION_GROUP_NONE)
      ePiece:SetSolid(SOLID_VPHYSICS)
      ePiece:SetMoveType(MOVETYPE_VPHYSICS)
      ePiece:SetNotSolid(false)
      ePiece:SetModel(sModel)
      ePiece:SetPos(vPos)
      ePiece:SetAngles(aAng)
      ePiece:Spawn()
      ePiece:Activate()
      ePiece:SetRenderMode(RENDERMODE_TRANSALPHA)
      ePiece:SetColor(stDrawDyes.White)
      ePiece:DrawShadow(true)
      ePiece:PhysWake()
      local phPiece = ePiece:GetPhysicsObject()
      if(phPiece and phPiece:IsValid()) then
        local IDs = trackasmlib.StringExplode(sBgSkIDs,"/")
        phPiece:SetMass(nMass)
        phPiece:EnableMotion(false)
        trackasmlib.AttachBodyGroups(ePiece,IDs[1] or "")
        ePiece:SetSkin(math.Clamp(tonumber(IDs[2]) or 0,0,ePiece:SkinCount()-1))
        trackasmlib.AttachAdditions(ePiece)
        return ePiece
      end
      ePiece:Remove()
      return nil
    end
    return nil
  end

  function ConstraintTrackAssemblyPiece(ePiece,eBase,nWe,nNc,nFr,nWg,nG,sP)
    if(ePiece and ePiece:IsValid()) then
      if(eBase and eBase:IsValid()) then
        if(nWe and nWe ~= 0) then
          local We = constraint.Weld(eBase, ePiece, 0, 0, 0, false, false)
          ePiece:DeleteOnRemove(We)
        end
        if(nNc and nNc ~= 0) then
          local Nc = constraint.NoCollide(eBase, ePiece, 0, 0)
          ePiece:DeleteOnRemove(Nc)
        end
      end
      local pyPiece = ePiece:GetPhysicsObject()
      if(pyPiece and pyPiece:IsValid()) then
        if(nFr and nFr == 0) then
          pyPiece:EnableMotion(true)
        end
        if(nWg and nWg ~= 0) then
          pyPiece:EnableMotion(false)
          ePiece:SetUnFreezable(true)
          ePiece.PhysgunDisabled = true
          duplicator.StoreEntityModifier(ePiece,trackasmlib.GetToolPrefixL().."weldgnd",{[1] = true })
        end
        if(not (nG and nG ~= 0)) then
          construct.SetPhysProp(nil,ePiece,0,pyPiece,{GravityToggle = false})
        end
        if(sP and sP ~= "") then
          construct.SetPhysProp(nil,ePiece,0,pyPiece,{Material = sP})
        end
      end
    end
  end
end

local function SnapAngleYaw(aAng, nYSnap)
  if(aAng and nYSnap and (nYSnap > 0)) then
    aAng[caY] = trackasmlib.SnapValue(aAng[caY],nYSnap)
  end
end

function TOOL:LeftClick(Trace)
  if(CLIENT) then return true end
  if(not Trace) then return false end
  if(not Trace.Hit) then return false end
  local trEnt     = Trace.Entity
  local model     = self:GetClientInfo("model") or ""
  local bgskids   = self:GetClientInfo("bgskids") or ""
  local physmater = self:GetClientInfo("physmater") or "metal"
  local freeze    = self:GetClientNumber("freeze") or 0
  local weld      = self:GetClientNumber("weld") or 0
  local wgnd      = self:GetClientNumber("weldgnd") or 0
  local nocolld   = self:GetClientNumber("nocollide") or 0
  local igntyp    = self:GetClientNumber("igntyp") or 0
  local flatten   = self:GetClientNumber("flatten") or 0
  local pointid   = self:GetClientNumber("pointid") or 1
  local pnextid   = self:GetClientNumber("pnextid") or 2
  local nextx     = self:GetClientNumber("nextx") or 0
  local nexty     = self:GetClientNumber("nexty") or 0
  local nextz     = self:GetClientNumber("nextz") or 0
  local mcspawn   = self:GetClientNumber("mcspawn") or 0
  local autoffsz  = self:GetClientNumber("autoffsz") or 0
  local engravity = self:GetClientNumber("engravity") or 0
  local count     = math.Clamp(self:GetClientNumber("count"),1,200)
  local mass      = math.Clamp(self:GetClientNumber("mass"),1,50000)
  local ydegsnp   = math.Clamp(self:GetClientNumber("ydegsnp"),0,180)
  local staatts   = math.Clamp(self:GetClientNumber("maxstaatts"),1,5)
  local actrad    = math.Clamp(self:GetClientNumber("activrad") or 1,1,150)
  local nextpic   = math.Clamp(self:GetClientNumber("nextpic") or 0,-360,360)
  local nextyaw   = math.Clamp(self:GetClientNumber("nextyaw") or 0,-360,360)
  local nextrol   = math.Clamp(self:GetClientNumber("nextrol") or 0,-360,360)
  local ply       = self:GetOwner()
  trackasmlib.PlyLoadKey(ply)
  if(Trace.HitWorld) then
  -- Spawn it on the map ...
    local ePiece = eMakeTrackAssemblyPiece(model,Trace.HitPos,ANG_ZERO,mass,bgskids)
    if(ePiece) then
      local aAng = ply:GetAimVector():Angle()
            aAng[caP] = 0
            aAng[caR] = 0
      SnapAngleYaw(aAng,ydegsnp)
      if(mcspawn ~= 0) then
        aAng[caP] = aAng[caP] + nextpic
        aAng[caY] = aAng[caY] - nextyaw
        aAng[caR] = aAng[caR] + nextrol
        ePiece:SetAngles(aAng)
        local vOffset = trackasmlib.GetMCWorldOffset(ePiece)
              vOffset[cvZ] = 0
        local vBBMin = ePiece:OBBMins()
        local vPos = Vector(Trace.HitPos[cvX] + nextx,
                            Trace.HitPos[cvY] + nexty,
                            Trace.HitPos[cvZ] - (Trace.HitNormal.z * vBBMin.z) + nextz)
        vPos:Add(vOffset)
        if(util.IsInWorld(vPos)) then
          ePiece:SetPos(vPos)
        else
          ePiece:Remove()
          trackasmlib.PrintNotify(ply,"Position out of map bounds!","ERROR")
          trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
          .."\n   Event  : Spawning when Trace.HitWorld"
          .."\n   MCspawn: "..mcspawn
          .."\n   Player : "..ply:Nick()
          .."\n   hdModel: "..trackasmlib.GetModelFileName(model).."\n")
          return false
        end
      else
        -- Spawn on Active point
        local stSpawn = trackasmlib.GetNORSpawn(Trace.HitPos,aAng,model,pointid,
                                                nextx,nexty,nextz,nextpic,nextyaw,nextrol)
        if(not stSpawn) then return false end
        if(autoffsz ~= 0) then
          local vAutoOff = trackasmlib.GetPntBBMinDistance(ePiece,stSpawn.HRec.Offs[pointid])
          stSpawn.SPos:Add(vAutoOff[cvZ] * Trace.HitNormal)
        end
        if(util.IsInWorld(stSpawn.SPos)) then
          ePiece:SetPos(stSpawn.SPos)
        else
          ePiece:Remove()
          trackasmlib.PrintNotify(ply,"Position out of map bounds!","ERROR")
          trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
          .."\n   Event  : Spawning when Trace.HitWorld"
          .."\n   MCspawn: "..mcspawn
          .."\n   Player : "..ply:Nick()
          .."\n   hdModel: "..trackasmlib.GetModelFileName(model).."\n")
          return false
        end
        ePiece:SetAngles(stSpawn.SAng)
      end
      undo.Create("Last Track Assembly")
      ConstraintTrackAssemblyPiece(ePiece,nil,weld,nocolld,freeze,wgnd,engravity,physmater)
      trackasmlib.EmitSoundPly(ply)
      undo.AddEntity(ePiece)
      undo.SetPlayer(ply)
      undo.SetCustomUndoText("Undone Assembly ( World Spawn )")
      undo.Finish()
      return true
    end
    return false
  end
  -- Hit Prop
  if(not util.IsValidModel(model)) then return false end
  if(not trEnt) then return false end
  if(not trEnt:IsValid()) then return false end
  if(not trackasmlib.IsPhysTrace(Trace)) then return false end
  if(trackasmlib.IsOther(trEnt)) then return false end

  local trModel = trEnt:GetModel()

  --No need stacking relative to non-persistent props or using them...
  local trRec   = trackasmlib.CacheQueryPiece(trModel)
  local hdRec   = trackasmlib.CacheQueryPiece(model)

  if(trackasmlib.PlyLoadKey(ply,"DUCK") and trRec) then
    -- IN_Use: Use the VALID Trace.Entity as a piece
    trackasmlib.PrintNotify(ply,"Model: "..trackasmlib.GetModelFileName(trModel).." selected !","GENERIC")
    ply:ConCommand(trackasmlib.GetToolPrefixL().."model "..trModel.."\n")
    ply:ConCommand(trackasmlib.GetToolPrefixL().."pointid 1\n")
    ply:ConCommand(trackasmlib.GetToolPrefixL().."pnextid 2\n")
    return true
  end

  if(hdRec and
     trRec and
     count > 1 and
     pointid ~= pnextid and
     trackasmlib.PlyLoadKey(ply,"SPEED")
  ) then
     -- IN_Speed: Switch the tool mode
    local stSpawn = trackasmlib.GetENTSpawn(trEnt,Trace.HitPos,
                                            model,pointid,actrad,
                                            flatten,igntyp,
                                            nextx,nexty,nextz,
                                            nextpic,nextyaw,nextrol)
    if(not stSpawn) then return false end
    if(stSpawn.HRec.Kept > 1) then
      local ePieceN, ePieceO
      local i       = count
      local nTrys   = staatts
      local vTemp   = Vector()
      local vLookAt = Vector()
      undo.Create("Last Track Assembly")
      ePieceO = trEnt
      while(i > 0) do
        ePieceN = eMakeTrackAssemblyPiece(model,ePieceO:GetPos(),ANG_ZERO,mass,bgskids)
        if(ePieceN) then
          if(util.IsInWorld(stSpawn.SPos)) then
            ePieceN:SetPos(stSpawn.SPos)
          else
            ePieceN:Remove()
            trackasmlib.PrintNotify(ply,"Position out of map bounds!","ERROR")
            trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
            .."\n   Event  : Stacking when available"
            .."\n   Iterats: "..tostring(count-i)
            .."\n   StackTr: "..tostring( nTrys ).." ?= "..tostring(staatts)
            .."\n   pointID: "..tostring(pointid).." >> "..tostring(pnextid)
            .."\n   Player : "..ply:Nick()
            .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
            .."\n   hdModel: "..trackasmlib.GetModelFileName(model).. "\n")
            trackasmlib.EmitSoundPly(ply)
            undo.SetPlayer(ply)
            undo.SetCustomUndoText("Undone Assembly ( Stack #"..tostring(count-i).." )")
            undo.Finish()
            return true
          end
          ePieceN:SetAngles(stSpawn.SAng)
          ConstraintTrackAssemblyPiece(ePieceN,ePieceO,weld,nocolld,freeze,wgnd,engravity,physmater)
          if(i == count) then
            if(not trackasmlib.IsThereID(stSpawn.HRec,pnextid)) then
              ePieceN:Remove()
              trackasmlib.PrintNotify(ply,"Cannot PointID data !","ERROR")
              trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
              .."\n   Event  : Non-existant PointID on client prop"
              .."\n   Iterats: "..tostring(count-i)
              .."\n   StackTr: "..tostring( nTrys ).." ?= "..tostring(staatts)
              .."\n   pointID: "..tostring(pointid).." >> "..tostring(pnextid)
              .."\n   Player : "..ply:Nick()
              .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
              .."\n   hdModel: "..trackasmlib.GetModelFileName(model).. "\n")
              return false
            end
            trackasmlib.SetVector(vLookAt,stSpawn.HRec.Offs[pnextid].P)
          end
          vTemp:Set(vLookAt)
          vTemp:Rotate(stSpawn.SAng)
          vTemp:Add(ePieceN:GetPos())
          undo.AddEntity(ePieceN)
          stSpawn = trackasmlib.GetENTSpawn(ePieceN,vTemp,model,
                                            pointid,actrad,
                                            flatten,igntyp,
                                            nextx,nexty,nextz,nextpic,nextyaw,nextrol)
          if(not stSpawn) then
            trackasmlib.PrintNotify(ply,"Cannot obtain spawn data!","ERROR")
            trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
            .."\n   Event  : Invalid User data"
            .."\n   Iterats: "..tostring(count-i)
            .."\n   StackTr: "..tostring( nTrys ).." ?= "..tostring(staatts)
            .."\n   pointID: "..tostring(pointid).." >> "..tostring(pnextid)
            .."\n   Player : "..ply:Nick()
            .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
            .."\n   hdModel: "..trackasmlib.GetModelFileName(model).. "\n")
            trackasmlib.EmitSoundPly(ply)
            undo.SetPlayer(ply)
            undo.SetCustomUndoText("Undone Assembly ( Stack #"..tostring(count-i).." )")
            undo.Finish()
            return true
          end
          ePieceO = ePieceN
          i = i - 1
          nTrys = staatts
        else
          nTrys = nTrys - 1
        end
        if(nTrys <= 0) then
          trackasmlib.PrintNotify(ply,"Spawn attemts ran off!","ERROR")
          trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
          .."\n   Event   : Failed to allocate memory for a piece"
          .."\n   Iterats: "..tostring(count-i)
          .."\n   StackTr: "..tostring( nTrys ).." ?= "..tostring(staatts)
          .."\n   pointID: "..tostring(pointid).." >> "..tostring(pnextid)
          .."\n   Player : "..ply:Nick()
          .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
          .."\n   hdModel: "..trackasmlib.GetModelFileName(model).. "\n")
          trackasmlib.EmitSoundPly(ply)
          undo.SetPlayer(ply)
          undo.SetCustomUndoText("Undone Assembly ( Stack #"..tostring(count-i).." )")
          undo.Finish()
          return true
        end
      end
      trackasmlib.EmitSoundPly(ply)
      undo.SetPlayer(ply)
      undo.SetCustomUndoText("Undone Assembly ( Stack #"..tostring(count-i).." )")
      undo.Finish()
      return true
    elseif(stSpawn.HRec.Kept == 1) then
      trackasmlib.Log(trackasmlib.GetToolNameU()..": Model "..model.." is non-stackable, spawning instead !!")
      ePiece = eMakeTrackAssemblyPiece(model,Trace.HitPos,ANG_ZERO,mass,bgskids)
      if(ePiece) then
        if(util.IsInWorld(stSpawn.SPos)) then
          ePiece:SetPos(stSpawn.SPos)
        else
          ePiece:Remove()
          trackasmlib.PrintNotify(ply,"Position out of map bounds !","ERROR")
          trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
          .."\n   Event  : Stacking when non available ( spawning instead )"
          .."\n   Player : "..ply:Nick()
          .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
          .."\n   hdModel: "..trackasmlib.GetModelFileName(model).. "\n")
          return true
        end
        ePiece:SetAngles(stSpawn.SAng)
        undo.Create("Last Track Assembly")
        ConstraintTrackAssemblyPiece(ePiece,trEnt,weld,nocolld,freeze,wgnd,engravity,physmater)
        trackasmlib.EmitSoundPly(ply)
        undo.AddEntity(ePiece)
        undo.SetPlayer(ply)
        undo.SetCustomUndoText("Undone Assembly ( Spawn Instead )")
        undo.Finish()
        return true
      end
    end
    return false
  end

  if(not( trRec and hdRec)) then return false end

  local stSpawn = trackasmlib.GetENTSpawn(trEnt,Trace.HitPos,
                                          model,pointid,actrad,
                                          flatten,igntyp,
                                          nextx,nexty,nextz,nextpic,nextyaw,nextrol)
  if(stSpawn) then
    local ePiece = eMakeTrackAssemblyPiece(model,Trace.HitPos,ANG_ZERO,mass,bgskids)
    if(ePiece) then
      if(util.IsInWorld(stSpawn.SPos)) then
        ePiece:SetPos(stSpawn.SPos)
      else
        ePiece:Remove()
        trackasmlib.PrintNotify(ply,"Position out of map bounds !","ERROR")
        trackasmlib.Log(trackasmlib.GetToolNameU()..": Additional Error INFO"
        .."\n   Event  : Spawn one piece relative to another"
        .."\n   Player : "..ply:Nick()
        .."\n   trModel: "..trackasmlib.GetModelFileName(trModel)
        .."\n   hdModel: "..trackasmlib.GetModelFileName(model).."\n")
        return true
      end
      ePiece:SetAngles(stSpawn.SAng)
      undo.Create("Last Track Assembly")
      ConstraintTrackAssemblyPiece(ePiece,trEnt,weld,nocolld,freeze,wgnd,engravity,physmater)
      trackasmlib.EmitSoundPly(ply)
      undo.AddEntity(ePiece)
      undo.SetPlayer(ply)
      undo.SetCustomUndoText("Undone Assembly ( Prop Relative )")
      undo.Finish()
      return true
    end
  elseif(trEnt) then
    local IDs = trackasmlib.StringExplode(bgskids,"/")
    trackasmlib.AttachBodyGroups(trEnt,"")  
    trackasmlib.AttachBodyGroups(trEnt,IDs[1] or "")
    trEnt:SetSkin(0)
    trEnt:SetSkin(math.Clamp(tonumber(IDs[2]) or 0,0,trEnt:SkinCount()-1))
    return true
  end
  return false
end

function TOOL:RightClick(Trace)
  -- Change the active point
  if(CLIENT) then return true end
  local model   = self:GetClientInfo("model")
  if(not util.IsValidModel(model)) then return false end
  local hdRec = trackasmlib.CacheQueryPiece(model)
  if(not hdRec) then return false end
  local pointid = self:GetClientNumber("pointid") or 0
  local pnextid = self:GetClientNumber("pnextid") or 0
  local ply     = self:GetOwner()
  local PointB  = pointid
  trackasmlib.PlyLoadKey(ply)
  if(Trace.HitWorld and trackasmlib.PlyLoadKey(ply,"USE")) then
    ply:ConCommand(trackasmlib.GetToolPrefixL().."openframe 20\n")
    return true
  end
  if(trackasmlib.PlyLoadKey(ply,"DUCK")) then -- Use
    if(trackasmlib.PlyLoadKey(ply,"SPEED")) then -- Left Shift
      pnextid = trackasmlib.IncDecNextID(pnextid,pointid,"-",hdRec)
    else
      pnextid = trackasmlib.IncDecNextID(pnextid,pointid,"+",hdRec)
    end
  else
    if(trackasmlib.PlyLoadKey(ply,"SPEED")) then -- Left Shift
      pointid = trackasmlib.IncDecPointID(pointid,"-",hdRec)
    else
      pointid = trackasmlib.IncDecPointID(pointid,"+",hdRec)
    end
  end
  if(pointid == pnextid) then
    pnextid = PointB
  end
  ply:ConCommand(trackasmlib.GetToolPrefixL().."pnextid "..pnextid.."\n")
  ply:ConCommand(trackasmlib.GetToolPrefixL().."pointid "..pointid.."\n")
end

function TOOL:Reload(Trace)
  if(CLIENT) then return true end
  if(not Trace) then return false end
  local ply       = self:GetOwner()
  trackasmlib.PlyLoadKey(ply)
  if(Trace.HitWorld) then
    if(trackasmlib.PlyLoadKey(ply,"SPEED")) then
      local exportdb  = self:GetClientNumber("exportdb") or 0
      local logsmax = self:GetClientNumber("logsmax") or 0
      if(logsmax > 0) then
        local logsenb   = self:GetClientNumber("logsenb") or 0
        local logfile = self:GetClientInfo  ("logfile") or ""
        trackasmlib.SetLogControl(logsenb,logsmax,logfile)
      end
      if(exportdb ~= 0) then
        trackasmlib.Log("TOOL:Reload(Trace) > Exporting DB")
        trackasmlib.ExportSQL2Lua    ("PIECES")
        trackasmlib.ExportSQL2Inserts("PIECES")
        trackasmlib.ExportSQL2Lua    ("ADDITIONS")
        trackasmlib.ExportSQL2Inserts("ADDITIONS")
        trackasmlib.ExportSQL2Lua    ("PHYSPROPERTIES")
        trackasmlib.ExportSQL2Inserts("PHYSPROPERTIES")
        trackasmlib.SQLExportIntoDSV ("PIECES","\t")
        trackasmlib.SQLExportIntoDSV ("ADDITIONS","\t")
        trackasmlib.SQLExportIntoDSV ("PHYSPROPERTIES","\t")
      end
    end
  end
  if(not trackasmlib.IsPhysTrace(Trace)) then return false end
  local trEnt = Trace.Entity
  if(trackasmlib.IsOther(trEnt)) then return false end
  local trRec = trackasmlib.CacheQueryPiece(trEnt:GetModel())
  if(trRec) then
    trEnt:Remove()
    return true
  end
  return false
end

function TOOL:Holster()
  self:ReleaseGhostEntity()
  if(self.GhostEntity and self.GhostEntity:IsValid()) then
    self.GhostEntity:Remove()
  end
end

local function DrawTextRowColor(xyPos,sTxT,stColor)
  -- Always Set the font before usage:
  -- e.g. surface.SetFont("Trebuchet18")
  if(not xyPos) then return end
  if(not (xyPos.x and xyPos.y)) then return end
  surface.SetTextPos(xyPos.x,xyPos.y)
  if(stColor) then
    surface.SetTextColor(stColor.r, stColor.g, stColor.b, stColor.a)
  end
  surface.DrawText(sTxT)
  xyPos.w, xyPos.h = surface.GetTextSize(sTxT)
  xyPos.y = xyPos.y + xyPos.h
end

local function DrawLineColor(xyPosS,xyPosE,nW,nH,stColor)
  if(not (xyPosS and xyPosE)) then return end
  if(not (xyPosS.x and xyPosS.y and xyPosE.x and xyPosE.y)) then return end
  if(stColor) then
    surface.SetDrawColor(stColor.r, stColor.g, stColor.b, stColor.a)
  end
  if(xyPosS.x < 0 or xyPosS.x > nW) then return end
  if(xyPosS.y < 0 or xyPosS.y > nH) then return end
  if(xyPosE.x < 0 or xyPosE.x > nW) then return end
  if(xyPosE.y < 0 or xyPosE.y > nH) then return end
  surface.DrawLine(xyPosS.x,xyPosS.y,xyPosE.x,xyPosE.y)
end

function TOOL:DrawHUD()
  if(SERVER) then return end
  local adv   = self:GetClientNumber("advise") or 0
  local ply   = LocalPlayer()
  local Trace = ply:GetEyeTrace()
  if(adv ~= 0) then
    local scrH = surface.ScreenHeight()
    local scrW = surface.ScreenWidth()
    if(not Trace) then return end
    local trEnt   = Trace.Entity
    local model   = self:GetClientInfo("model")
    local nextx   = self:GetClientNumber("nextx") or 0
    local nexty   = self:GetClientNumber("nexty") or 0
    local nextz   = self:GetClientNumber("nextz") or 0
    local pointid = self:GetClientNumber("pointid") or 0
    local pnextid = self:GetClientNumber("pnextid") or 0
    local nextpic = math.Clamp(self:GetClientNumber("nextpic") or 0,-360,360)
    local nextyaw = math.Clamp(self:GetClientNumber("nextyaw") or 0,-360,360)
    local nextrol = math.Clamp(self:GetClientNumber("nextrol") or 0,-360,360)
    if(trEnt and trEnt:IsValid()) then
      if(trackasmlib.IsOther(trEnt)) then return end
      local actrad  = math.Clamp(self:GetClientNumber("activrad") or 1,1,150)
      local igntyp  = self:GetClientNumber("igntyp") or 0
      local flatten = self:GetClientNumber("flatten") or 0
      local stSpawn = trackasmlib.GetENTSpawn(trEnt,Trace.HitPos,
                                              model,pointid,actrad,
                                              flatten,igntyp,
                                              nextx,nexty,nextz,
                                              nextpic,nextyaw,nextrol)
      if(not stSpawn) then return end
      local addinfo = self:GetClientNumber("addinfo") or 0
      stSpawn.F:Mul(30)
      stSpawn.F:Add(stSpawn.OPos)
      stSpawn.R:Mul(30)
      stSpawn.R:Add(stSpawn.OPos)
      stSpawn.U:Mul(30)
      stSpawn.U:Add(stSpawn.OPos)
      local RadScale = math.Clamp(75 * stSpawn.RLen / (Trace.HitPos - ply:GetPos()):Length(),1,100)
      local Os = stSpawn.OPos:ToScreen()
      local Ss = stSpawn.SPos:ToScreen()
      local Xs = stSpawn.F:ToScreen()
      local Ys = stSpawn.R:ToScreen()
      local Zs = stSpawn.U:ToScreen()
      local Pp = stSpawn.PPos:ToScreen()
      if(stSpawn.HRec.Offs[pnextid] and stSpawn.HRec.Kept > 1) then
        local vNext = Vector()
              trackasmlib.SetVector(vNext,stSpawn.HRec.Offs[pnextid].O)
              vNext:Rotate(stSpawn.SAng)
              vNext:Add(stSpawn.SPos)
        local Np = vNext:ToScreen()
        -- Draw Next Point
        DrawLineColor(Os,Np,scrW,scrH,stDrawDyes.Yello)
        surface.DrawCircle( Np.x, Np.y, RadScale / 2, stDrawDyes.Green)
      end
      -- Draw UCS
      DrawLineColor(Os,Xs,scrW,scrH,stDrawDyes.Red  )
      DrawLineColor(Os,Ys,scrW,scrH,stDrawDyes.Green)
      DrawLineColor(Os,Zs,scrW,scrH,stDrawDyes.Blue )
      surface.DrawCircle( Os.x, Os.y, RadScale, stDrawDyes.Yello)
      -- Draw Spawn
      DrawLineColor(Os,Ss,scrW,scrH,stDrawDyes.Magen)
      DrawLineColor(Os,Pp,scrW,scrH,stDrawDyes.Red  )
      surface.DrawCircle( Ss.x, Ss.y, RadScale,     stDrawDyes.Cyan)
      surface.DrawCircle( Pp.x, Pp.y, RadScale / 2, stDrawDyes.Red)
      if(addinfo ~= 0) then
        local txPos = {x = 0, y = 0, w = 0, h = 0}
        txPos.x = surface.ScreenWidth() / 2 + 10
        txPos.y = surface.ScreenHeight()/ 2 + 10
        surface.SetFont("Trebuchet18")
        DrawTextRowColor(txPos,"Act Rad: "..tostring(stSpawn.RLen),stDrawDyes.Black)
        DrawTextRowColor(txPos,"Mod POS: "..tostring(stSpawn.MPos))
        DrawTextRowColor(txPos,"Mod ANG: "..tostring(stSpawn.MAng))
        DrawTextRowColor(txPos,"Spn POS: "..tostring(stSpawn.SPos))
        DrawTextRowColor(txPos,"Spn ANG: "..tostring(stSpawn.SAng))
      end
    elseif(Trace.HitWorld) then
      local mcspawn  = self:GetClientNumber("mcspawn") or 0
      local autoffsz = self:GetClientNumber("autoffsz") or 0
      local ydegsnp  = math.Clamp(self:GetClientNumber("ydegsnp"),0,180)
      local addinfo = self:GetClientNumber("addinfo") or 0
      if(mcspawn ~= 0) then -- Relative to MC
        local RadScale = math.Clamp(1500 / (Trace.HitPos - ply:GetPos()):Length(),1,100)
        local aAng = ply:GetAimVector():Angle()
              aAng[caP] = 0
              aAng[caR] = 0
        SnapAngleYaw(aAng,ydegsnp)
        aAng[caP] = aAng[caP] + nextpic
        aAng[caY] = aAng[caY] - nextyaw
        aAng[caR] = aAng[caR] + nextrol
        local vPos = Trace.HitPos
        local F = aAng:Forward()
              F:Mul(30)
              F:Add(vPos)
        local R = aAng:Right()
              R:Mul(30)
              R:Add(vPos)
        local U = aAng:Up()
              U:Mul(30)
              U:Add(vPos)
        local Os = vPos:ToScreen()
        local Xs = F:ToScreen()
        local Ys = R:ToScreen()
        local Zs = U:ToScreen()
        DrawLineColor(Os,Xs,scrW,scrH,stDrawDyes.Red  )
        DrawLineColor(Os,Ys,scrW,scrH,stDrawDyes.Green)
        DrawLineColor(Os,Zs,scrW,scrH,stDrawDyes.Blue )
        surface.DrawCircle( Os.x, Os.y, RadScale, stDrawDyes.Yello)
        if(addinfo ~= 0) then
          local txPos = {x = 0, y = 0, w = 0, h = 0}
          txPos.x = surface.ScreenWidth() / 2 + 10
          txPos.y = surface.ScreenHeight()/ 2 + 10
          surface.SetFont("Trebuchet18")
          DrawTextRowColor(txPos,"Mod POS: " ..tostring(vPos),stDrawDyes.Black)
          DrawTextRowColor(txPos,"Mod ANG: " ..tostring(aAng))
        end
      else -- Relative to the active Point
        if(pointid > 0 and pnextid > 0) then
          local RadScale = math.Clamp(1500 / (Trace.HitPos - ply:GetPos()):Length(),1,100)
          local aAng = ply:GetAimVector():Angle()
                aAng[caP] = 0
                aAng[caR] = 0
          SnapAngleYaw(aAng,ydegsnp)
          local stSpawn = trackasmlib.GetNORSpawn(Trace.HitPos,aAng,model,pointid,
                                                  nextx,nexty,nextz,nextpic,nextyaw,nextrol)
          if(stSpawn) then
            stSpawn.F:Mul(30)
            stSpawn.F:Add(stSpawn.OPos)
            stSpawn.R:Mul(30)
            stSpawn.R:Add(stSpawn.OPos)
            stSpawn.U:Mul(30)
            stSpawn.U:Add(stSpawn.OPos)
            local Os = stSpawn.OPos:ToScreen()
            local Ss = stSpawn.SPos:ToScreen()
            local Xs = stSpawn.F:ToScreen()
            local Ys = stSpawn.R:ToScreen()
            local Zs = stSpawn.U:ToScreen()
            local Pp = stSpawn.PPos:ToScreen()
            if(stSpawn.HRec.Kept > 1 and stSpawn.HRec.Offs[pnextid]) then
              local vNext = Vector()
                    trackasmlib.SetVector(vNext,stSpawn.HRec.Offs[pnextid].O)
                    vNext:Rotate(stSpawn.SAng)
                    vNext:Add(stSpawn.SPos)
              local Np = vNext:ToScreen()
              -- Draw Next Point
              DrawLineColor(Os,Np,scrW,scrH,stDrawDyes.Yello)
              surface.DrawCircle( Np.x, Np.y, RadScale / 2, stDrawDyes.Green)
            end
            -- Draw UCS
            DrawLineColor(Os,Xs,scrW,scrH,stDrawDyes.Red  )
            DrawLineColor(Os,Ys,scrW,scrH,stDrawDyes.Green)
            DrawLineColor(Os,Zs,scrW,scrH,stDrawDyes.Blue )
            surface.DrawCircle( Os.x, Os.y, RadScale, stDrawDyes.Yello)
            -- Draw Spawn
            DrawLineColor(Os,Ss,scrW,scrH,stDrawDyes.Magen)
            DrawLineColor(Os,Pp,scrW,scrH,stDrawDyes.Red  )
            surface.DrawCircle( Ss.x, Ss.y, RadScale, stDrawDyes.Cyan)
            surface.DrawCircle( Pp.x, Pp.y, RadScale / 2, stDrawDyes.Red)
            if(addinfo ~= 0) then
              local txPos = {x = 0, y = 0, w = 0, h = 0}
              txPos.x = surface.ScreenWidth() / 2 + 10
              txPos.y = surface.ScreenHeight()/ 2 + 10
              surface.SetFont("Trebuchet18")
              DrawTextRowColor(txPos,"Origin : " ..tostring(stSpawn.OPos),stDrawDyes.Black)
              DrawTextRowColor(txPos,"Mod POS: " ..tostring(stSpawn.MPos))
              DrawTextRowColor(txPos,"Mod ANG: " ..tostring(stSpawn.MAng))
              DrawTextRowColor(txPos,"Spn POS: " ..tostring(stSpawn.SPos))
              DrawTextRowColor(txPos,"Spn ANG: " ..tostring(stSpawn.SAng))
            end
          end
        end
      end
    end
  end
end

function TOOL:DrawToolScreen(w, h)
  if(SERVER) then return end
  surface.SetTexture(txToolgunBackground)
  surface.SetDrawColor(stDrawDyes.Black)
  surface.DrawTexturedRect(0, 0, w, h)
  surface.SetFont("Trebuchet24")
  local Trace = LocalPlayer():GetEyeTrace()
  local txPos = {x = 0, y = 0, w = 0, h = 0}
  if(not Trace) then
    DrawTextRowColor(txPos,"Trace status: Invalid",stDrawDyes.White)
    return
  end
  DrawTextRowColor(txPos,"Trace status: Valid",stDrawDyes.White)
  local model = self:GetClientInfo("model")
  if(not util.IsValidModel(model)) then
    DrawTextRowColor(txPos,"Holds Model: Invalid")
    return
  end
  local hdRec = trackasmlib.CacheQueryPiece(model)
  if(not hdRec) then
    DrawTextRowColor(txPos,"Holds Model: Invalid")
    return
  end
  DrawTextRowColor(txPos,"Holds Model: Valid")
  local NoID    = "N"
  local NoAV    = "N/A"
  local pointid = self:GetClientNumber("pointid") or 0
  local pnextid = self:GetClientNumber("pnextid") or 0
  local actrad  = math.Clamp(self:GetClientNumber("activrad") or 1,1,150)
  local trEnt   = Trace.Entity
  local trMaxCN, trModel, trOID, trRLen
  if(trEnt and trEnt:IsValid()) then
    if(trackasmlib.IsOther(trEnt)) then return end
          trModel = trEnt:GetModel()
    local trRec   = trackasmlib.CacheQueryPiece(trModel)
    local nextx   = self:GetClientNumber("nextx") or 0
    local nexty   = self:GetClientNumber("nexty") or 0
    local nextz   = self:GetClientNumber("nextz") or 0
    local igntyp  = self:GetClientNumber("igntyp") or 0
    local flatten = self:GetClientNumber("flatten") or 0
    local nextpic = math.Clamp(self:GetClientNumber("nextpic") or 0,-360,360)
    local nextyaw = math.Clamp(self:GetClientNumber("nextyaw") or 0,-360,360)
    local nextrol = math.Clamp(self:GetClientNumber("nextrol") or 0,-360,360)
    local stSpawn = trackasmlib.GetENTSpawn(trEnt,Trace.HitPos,
                                            model,pointid,actrad,
                                            flatten,igntyp,
                                            nextx,nexty,nextz,
                                            nextpic,nextyaw,nextrol)
    if(stSpawn) then
      trOID  = stSpawn.OID
      trRLen = trackasmlib.RoundValue(stSpawn.RLen,0.01)
    end
    if(trRec) then
      trMaxCN = trRec.Kept
      trModel = trackasmlib.GetModelFileName(trModel)
    else
      trModel = "[X]"..trackasmlib.GetModelFileName(trModel)
    end
  end
  actrad = trackasmlib.RoundValue(actrad,0.01)
  DrawTextRowColor(txPos,"HM: " ..trackasmlib.GetModelFileName(model))
  DrawTextRowColor(txPos,"TM: " ..(trModel    or NoAV))
  DrawTextRowColor(txPos,"ID: ["..(trMaxCN    or NoID)
                        .."] "  ..(trOID      or NoID)
                        .." >> "..(pointid    or NoID)
                        .. " (" ..(pnextid    or NoID)
                        ..") [" ..(hdRec.Kept or NoID)
                        .."]")
  DrawTextRowColor(txPos,"CurAR: "..(trRLen or NoAV),stDrawDyes.Yello)
  DrawTextRowColor(txPos,"MaxCL: "..actrad.."<[150.00]",stDrawDyes.Cyan)
  local nRad = math.Clamp(h - txPos.y - txPos.h,0,256) / 2
  local cPos = math.Clamp(h - nRad - (txPos.h  / 2),0,256)
  if(trRLen) then
    surface.DrawCircle( cPos, cPos, math.Clamp(trRLen/150,0,1)*nRad, stDrawDyes.Yello)
  end
  local sTime = tostring(os.date())
  surface.DrawCircle( cPos, cPos, math.Clamp(actrad/150,0,1)*nRad, stDrawDyes.Cyan)
  surface.DrawCircle( cPos, cPos, nRad, stDrawDyes.Magen)
  DrawTextRowColor(txPos,string.sub(sTime,1,8),stDrawDyes.White)
  DrawTextRowColor(txPos,string.sub(sTime,10,17))
end

function TOOL.BuildCPanel(CPanel)
  Header = CPanel:AddControl( "Header", { Text        = "#Tool.trackassembly.Name",
                                          Description = "#Tool.trackassembly.desc" })
  local CurY = Header:GetTall() + 2

  local Combo         = {}
  Combo["Label"]      = "#Presets"
  Combo["MenuButton"] = "1"
  Combo["Folder"]     = trackasmlib.GetToolNameL()
  Combo["CVars"]      = {}
  Combo["CVars"][ 0]  = trackasmlib.GetToolPrefixL().."weld"
  Combo["CVars"][ 1]  = trackasmlib.GetToolPrefixL().."mass"
  Combo["CVars"][ 2]  = trackasmlib.GetToolPrefixL().."model"
  Combo["CVars"][ 3]  = trackasmlib.GetToolPrefixL().."nextx"
  Combo["CVars"][ 4]  = trackasmlib.GetToolPrefixL().."nexty"
  Combo["CVars"][ 5]  = trackasmlib.GetToolPrefixL().."nextz"
  Combo["CVars"][ 6]  = trackasmlib.GetToolPrefixL().."count"
  Combo["CVars"][ 7]  = trackasmlib.GetToolPrefixL().."freeze"
  Combo["CVars"][ 8]  = trackasmlib.GetToolPrefixL().."advise"
  Combo["CVars"][ 9]  = trackasmlib.GetToolPrefixL().."igntyp"
  Combo["CVars"][10]  = trackasmlib.GetToolPrefixL().."weldgnd"
  Combo["CVars"][11]  = trackasmlib.GetToolPrefixL().."flatten"
  Combo["CVars"][13]  = trackasmlib.GetToolPrefixL().."pointid"
  Combo["CVars"][14]  = trackasmlib.GetToolPrefixL().."pnextid"
  Combo["CVars"][15]  = trackasmlib.GetToolPrefixL().."nextpic"
  Combo["CVars"][16]  = trackasmlib.GetToolPrefixL().."nextyaw"
  Combo["CVars"][17]  = trackasmlib.GetToolPrefixL().."nextrol"
  Combo["CVars"][18]  = trackasmlib.GetToolPrefixL().."enghost"
  Combo["CVars"][19]  = trackasmlib.GetToolPrefixL().."ydegsnp"
  Combo["CVars"][20]  = trackasmlib.GetToolPrefixL().."mcspawn"
  Combo["CVars"][21]  = trackasmlib.GetToolPrefixL().."activrad"
  Combo["CVars"][22]  = trackasmlib.GetToolPrefixL().."nocollide"
  Combo["CVars"][23]  = trackasmlib.GetToolPrefixL().."engravity"
  Combo["CVars"][24]  = trackasmlib.GetToolPrefixL().."physmater"
  CPanel:AddControl("ComboBox",Combo)
  CurY = CurY + 25
  local Sorted = trackasmlib.CacheQueryPanel()
  local defTable = trackasmlib.GetTableDefinition("PIECES")
  local pTree = vgui.Create("DTree")
        pTree:SetPos(2, CurY)
        pTree:SetSize(2, 250)
        pTree:SetIndentSize(0)
  local pFolders = {}
  local pNode
  local pItem
  local Cnt = 1
  while(Sorted[Cnt]) do
    local v     = Sorted[Cnt]
    local Model = v[defTable[1][1]]
    local Type  = v[defTable[2][1]]
    local Name  = v[defTable[3][1]]
    if(file.Exists(Model, "GAME")) then
      if(Type ~= "" and not pFolders[Type]) then
      -- No Folder, Make one xD
        pItem = pTree:AddNode(Type)
        pItem:SetName(Type)
        pItem.Icon:SetImage("icon16/disconnect.png")
        function pItem:InternalDoClick() end
          pItem.DoClick = function()
          return false
        end
        local FolderLabel = pItem.Label
        function FolderLabel:UpdateColours(skin)
          return self:SetTextStyleColor(stDrawDyes.Txtsk)
        end
        pFolders[Type] = pItem
      end
      if(pFolders[Type]) then
        pItem = pFolders[Type]
      else
        pItem = pTree
      end
      pNode = pItem:AddNode(Name)
      pNode:SetName(Name)
      pNode.Icon:SetImage("icon16/control_play_blue.png")
      pNode.DoClick = function()
        RunConsoleCommand(trackasmlib.GetToolPrefixL().."model"  , Model)
        RunConsoleCommand(trackasmlib.GetToolPrefixL().."pointid", 1)
        RunConsoleCommand(trackasmlib.GetToolPrefixL().."pnextid", 2)
      end
    else
      trackasmlib.PrintInstance(trackasmlib.GetToolNameU()..": Model "
             .. Model
             .. " is not available in"
             .. " your system .. SKIPPING !")
    end
    Cnt = Cnt + 1
  end
  CPanel:AddItem(pTree)
  CurY = CurY + pTree:GetTall() + 2
  trackasmlib.PrintInstance(trackasmlib.GetToolNameU()..": Found #"..tostring(Cnt-1).." piece items.")

  -- http://wiki.garrysmod.com/page/Category:DComboBox
  local pComboPhysType = vgui.Create("DComboBox")
        pComboPhysType:SetPos(2, CurY)
        pComboPhysType:SetTall(18)
        pComboPhysType:SetValue("<Select Surface Material TYPE>")
        CurY = CurY + pComboPhysType:GetTall() + 2
  local pComboPhysName = vgui.Create("DComboBox")
        pComboPhysName:SetPos(2, CurY)
        pComboPhysName:SetTall(18)
        pComboPhysName:SetValue(trackasmlib.GetDefaultString(GetConVarString("gearassembly_physmater"),
                                                                     "<Select Surface Material NAME>"))
        CurY = CurY + pComboPhysName:GetTall() + 2
  local defTable = trackasmlib.GetTableDefinition("PHYSPROPERTIES")
  local Sorted = trackasmlib.PanelCacheQueryProperties()
  local CntTyp = 1
  local qNames
  while(Sorted[CntTyp]) do
    local v   = Sorted[CntTyp]
    local Typ = v[defTable[2][1]]
    pComboPhysType:AddChoice(Typ)
    pComboPhysType.OnSelect = function(pnSelf, nInd, sVal)
      qNames = trackasmlib.PanelCacheQueryProperties(sVal)
      if(qNames) then
        pComboPhysName:Clear()
        pComboPhysName:SetValue("<Select Surface Material NAME>")
        local CntNam = 1
        while(qNames[CntNam]) do
          local v   = qNames[CntNam]
          local Nam = v[defTable[3][1]]
          pComboPhysName:AddChoice(Nam)
          pComboPhysName.OnSelect = function(pnSelf, nInd, sVal)
            RunConsoleCommand(trackasmlib.GetToolPrefixL().."physmater", sVal)
          end
          CntNam = CntNam + 1
        end
      end
    end
    CntTyp = CntTyp + 1
  end
  CPanel:AddItem(pComboPhysType)
  CPanel:AddItem(pComboPhysName)
  trackasmlib.PrintInstance(trackasmlib.GetToolNameU()..": Found #"..(CntTyp-1).." material types.")

  -- http://wiki.garrysmod.com/page/Category:DTextEntry
  local pText = vgui.Create("DTextEntry")
        pText:SetPos(2, 300)
        pText:SetTall(18)
        pText:SetText(trackasmlib.GetDefaultString(GetConVarString(trackasmlib.GetToolPrefixL().."bgskids"),
                                             "Comma delimited Body/Skin IDs > ENTER ( INS to Auto-fill from Trace )"))
        pText.OnKeyCodeTyped = function(pnSelf, nKeyEnum)
          if(nKeyEnum == KEY_INSERT ) then
            local sTX = trackasmlib.GetBodygroupTrace()
                 .."/"..trackasmlib.GetSkinTrace()
            pnSelf:SetText(sTX)
            pnSelf:SetValue(sTX)
          elseif(nKeyEnum == KEY_ENTER) then
            local sTX = pnSelf:GetValue() or ""
            RunConsoleCommand(trackasmlib.GetToolPrefixL().."bgskids",sTX)
          end
        end
        CurY = CurY + pText:GetTall() + 2
        
  CPanel:AddItem(pText)

  CPanel:AddControl("Slider", {
            Label   = "Piece mass: ",
            Type    = "Integer",
            Min     = 1,
            Max     = 50000,
            Command = trackasmlib.GetToolPrefixL().."mass"})

  CPanel:AddControl("Slider", {
            Label   = "Active radius: ",
            Type    = "Float",
            Min     = 1,
            Max     = 150,
            Command = trackasmlib.GetToolPrefixL().."activrad"})

  CPanel:AddControl("Slider", {
            Label   = "Pieces count: ",
            Type    = "Integer",
            Min     = 1,
            Max     = 200,
            Command = trackasmlib.GetToolPrefixL().."count"})

  CPanel:AddControl("Slider", {
            Label   = "Yaw snap amount: ",
            Type    = "Float",
            Min     = 0,
            Max     = 180,
            Command = trackasmlib.GetToolPrefixL().."ydegsnp"})

  CPanel:AddControl("Button", {
            Label   = "V Reset Offset Values V",
            Command = trackasmlib.GetToolPrefixL().."resetoffs",
            Text    = "Reset All Offsets" })

  CPanel:AddControl("Slider", {
            Label   = "UCS Pitch: ",
            Type    = "Float",
            Min     = -360,
            Max     =  360,
            Command = trackasmlib.GetToolPrefixL().."nextpic"})

  CPanel:AddControl("Slider", {
            Label   = "UCS Yaw: ",
            Type    = "Float",
            Min     = -360,
            Max     =  360,
            Command = trackasmlib.GetToolPrefixL().."nextyaw"})

  CPanel:AddControl("Slider", {
            Label   = "Piece Roll: ",
            Type    = "Float",
            Min     = -360,
            Max     =  360,
            Command = trackasmlib.GetToolPrefixL().."nextrol"})

  CPanel:AddControl("Slider", {
            Label   = "Offset X: ",
            Type    = "Float",
            Min     = -10000,
            Max     =  10000,
            Command = trackasmlib.GetToolPrefixL().."nextx"})

  CPanel:AddControl("Slider", {
            Label = "Offset Y: ",
            Type  = "Float",
            Min   = -10000,
            Max   =  10000,
            Command = trackasmlib.GetToolPrefixL().."nexty"})

  CPanel:AddControl("Slider", {
            Label   = "Offset Z: ",
            Type    = "Float",
            Min     = -10000,
            Max     =  10000,
            Command = trackasmlib.GetToolPrefixL().."nextz"})

  CPanel:AddControl("Checkbox", {
            Label   = "Enable pieces gravity",
            Command = trackasmlib.GetToolPrefixL().."engravity"})

  CPanel:AddControl("Checkbox", {
            Label   = "Weld pieces",
            Command = trackasmlib.GetToolPrefixL().."weld"})

  CPanel:AddControl("Checkbox", {
            Label   = "Weld to the ground",
            Command = trackasmlib.GetToolPrefixL().."weldgnd"})

  CPanel:AddControl("Checkbox", {
            Label   = "NoCollide pieces",
            Command = trackasmlib.GetToolPrefixL().."nocollide"})

  CPanel:AddControl("Checkbox", {
            Label   = "Freeze pieces",
            Command = trackasmlib.GetToolPrefixL().."freeze"})

  CPanel:AddControl("Checkbox", {
            Label   = "Ignore track type",
            Command = trackasmlib.GetToolPrefixL().."igntyp"})

  CPanel:AddControl("Checkbox", {
            Label   = "Next piece flat to surface",
            Command = trackasmlib.GetToolPrefixL().."flatten"})

  CPanel:AddControl("Checkbox", {
            Label   = "Origin from mass-centre",
            Command = trackasmlib.GetToolPrefixL().."mcspawn"})

  CPanel:AddControl("Checkbox", {
            Label   = "Auto-offset point Z",
            Command = trackasmlib.GetToolPrefixL().."autoffsz"})

  CPanel:AddControl("Checkbox", {
            Label   = "Enable advisor",
            Command = trackasmlib.GetToolPrefixL().."advise"})

  CPanel:AddControl("Checkbox", {
            Label   = "Enable ghosting",
            Command = trackasmlib.GetToolPrefixL().."enghost"})
end

function TOOL:MakeGhostEntity(sModel,vPos,aAngle)
  -- Check for invalid model
  if(not util.IsValidModel(sModel)) then return end
  util.PrecacheModel(sModel)
  -- We do ghosting serverside in single player
  -- It's done clientside in multiplayer
  if(SERVER and not game.SinglePlayer()) then return end
  if(CLIENT and     game.SinglePlayer()) then return end
  -- Release the old ghost entity
  self:ReleaseGhostEntity()
  if(CLIENT) then
    self.GhostEntity = ents.CreateClientProp(sModel)
  else
    if(util.IsValidRagdoll(sModel)) then
      self.GhostEntity = ents.Create("prop_dynamic")
    else
      self.GhostEntity = ents.Create("prop_physics")
    end
  end
  -- If there are too many entities we might not spawn..
  if(not self.GhostEntity:IsValid()) then
    self.GhostEntity = nil
    return
  end
  self.GhostEntity:SetModel(sModel)
  self.GhostEntity:SetPos(vPos)
  self.GhostEntity:SetAngles(aAngle)
  self.GhostEntity:Spawn()
  self.GhostEntity:SetSolid(SOLID_VPHYSICS);
  self.GhostEntity:SetMoveType(MOVETYPE_NONE)
  self.GhostEntity:SetNotSolid(true);
  self.GhostEntity:SetRenderMode(RENDERMODE_TRANSALPHA)
  self.GhostEntity:SetColor(stDrawDyes.Ghost)
end

function TOOL:UpdateGhost(oEnt, Ply)
  if(not (oEnt and oEnt:IsValid())) then return end
  oEnt:SetNoDraw(true)
  local Trace = util.TraceLine(util.GetPlayerTrace(Ply))
  if(not Trace) then return end
  local trEnt = Trace.Entity
  if(Trace.HitWorld) then
    local model     = self:GetClientInfo("model") or ""
    local nextx     = self:GetClientNumber("nextx") or 0
    local nexty     = self:GetClientNumber("nexty") or 0
    local nextz     = self:GetClientNumber("nextz") or 0
    local pointid   = self:GetClientNumber("pointid") or 1
    local mcspawn   = self:GetClientNumber("mcspawn") or 0
    local ydegsnp   = math.Clamp(self:GetClientNumber("ydegsnp"),0,180)
    local nextpic   = math.Clamp(self:GetClientNumber("nextpic") or 0,-360,360)
    local nextyaw   = math.Clamp(self:GetClientNumber("nextyaw") or 0,-360,360)
    local nextrol   = math.Clamp(self:GetClientNumber("nextrol") or 0,-360,360)
    local autoffsz  = self:GetClientNumber("autoffsz") or 0
    local aAng      = Ply:GetAimVector():Angle()
          aAng[caP] = 0
          aAng[caR] = 0
    SnapAngleYaw(aAng,ydegsnp)
    if(mcspawn ~= 0) then
      aAng[caP] = aAng[caP] + nextpic
      aAng[caY] = aAng[caY] - nextyaw
      aAng[caR] = aAng[caR] + nextrol
      oEnt:SetAngles(aAng)
      local vBBMin  = oEnt:OBBMins()
      local vPos    = Vector(Trace.HitPos[cvX],
                             Trace.HitPos[cvY],
                             Trace.HitPos[cvZ] - (Trace.HitNormal[cvZ] * vBBMin[cvZ]))
      local vOff = trackasmlib.GetMCWorldOffset(oEnt)
            vOff[cvX] = vOff[cvX] + nextx
            vOff[cvY] = vOff[cvY] + nexty
            vOff[cvZ] = nextz
      vPos:Add(vOff)
      oEnt:SetPos(vPos)
    else
      local stSpawn = trackasmlib.GetNORSpawn(Trace.HitPos,aAng,model,pointid,
                                              nextx,nexty,nextz,nextpic,nextyaw,nextrol)
      if(stSpawn) then
      oEnt:SetNoDraw(false)
        if(autoffsz ~= 0) then
          local vAutoOff = trackasmlib.GetPntBBMinDistance(oEnt,stSpawn.HRec.Offs[pointid])
          stSpawn.SPos:Add(vAutoOff[cvZ] * Trace.HitNormal)
        end
        oEnt:SetAngles(stSpawn.SAng)
        oEnt:SetPos(stSpawn.SPos)
      end
    end
  elseif(trEnt and trEnt:IsValid()) then
    if(trackasmlib.IsOther(trEnt)) then return end
    local trRec = trackasmlib.CacheQueryPiece(trEnt:GetModel())
    if(trRec) then
      local model   = self:GetClientInfo("model")
      local nextx   = self:GetClientNumber("nextx") or 0
      local nexty   = self:GetClientNumber("nexty") or 0
      local nextz   = self:GetClientNumber("nextz") or 0
      local igntyp  = self:GetClientNumber("igntyp") or 0
      local flatten = self:GetClientNumber("flatten") or 0
      local pointid = self:GetClientNumber("pointid") or 0
      local actrad  = math.Clamp(self:GetClientNumber("activrad") or 1,1,150)
      local nextpic = math.Clamp(self:GetClientNumber("nextpic") or 0,-360,360)
      local nextyaw = math.Clamp(self:GetClientNumber("nextyaw") or 0,-360,360)
      local nextrol = math.Clamp(self:GetClientNumber("nextrol") or 0,-360,360)
      local stSpawn = trackasmlib.GetENTSpawn(trEnt,Trace.HitPos,model,
                                              pointid,actrad,
                                              flatten,igntyp,
                                              nextx,nexty,nextz,
                                              nextpic,nextyaw,nextrol)
      if(stSpawn) then
        oEnt:SetNoDraw(false)
        oEnt:SetPos(stSpawn.SPos)
        oEnt:SetAngles(stSpawn.SAng)
      end
    end
  end
end

function TOOL:Think()
  local model = self:GetClientInfo("model")
  if((tonumber(self:GetClientInfo("enghost")) or 0) ~= 0 and
      util.IsValidModel(model)
  ) then
    if (not self.GhostEntity or
        not self.GhostEntity:IsValid() or
            self.GhostEntity:GetModel() ~= model
    ) then
      -- If none ...
      self:MakeGhostEntity(model, VEC_ZERO, ANG_ZERO)
    end
    self:UpdateGhost(self.GhostEntity, self:GetOwner())
  else
    self:ReleaseGhostEntity()
    if(self.GhostEntity and self.GhostEntity:IsValid()) then
      self.GhostEntity:Remove()
    end
  end
end
